function encriptar(){
    var texto = document.getElementById("texto").value;
    var resultado = encriptarTexto(texto);
    var divResultado = document.querySelector(".resultado");
    var parrafos = divResultado.querySelectorAll("p");
    parrafos[0].textContent = "";
    parrafos[1].textContent = resultado;
}

function encriptarTexto(texto){
    texto = texto.replace(/e/g, "enter");
    texto = texto.replace(/i/g, "imes");
    texto = texto.replace(/a/g, "ai");
    texto = texto.replace(/o/g, "ober");
    texto = texto.replace(/u/g, "ufat");
    return texto;
}

function desencriptar(){
    var texto = document.getElementById("texto").value;
    var resultado = desencriptarTexto(texto);
    var divResultado = document.querySelector(".resultado");
    var parrafos = divResultado.querySelectorAll("p");
    parrafos[0].textContent = "";
    parrafos[1].textContent = resultado;
}

function desencriptarTexto(texto){
    texto = texto.replace(/enter/g, "e");
    texto = texto.replace(/imes/g, "i");
    texto = texto.replace(/ai/g, "a");
    texto = texto.replace(/ober/g, "o");
    texto = texto.replace(/ufat/g, "u");
    return texto;
}
