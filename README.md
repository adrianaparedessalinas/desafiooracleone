# Aplicación de Encriptación de Textos

Esta es una aplicación web simple que te permite encriptar textos utilizando algoritmos de encriptación en JavaScript.

## Características

- Encripta cualquier texto ingresado por el usuario.
- Utiliza algoritmos de encriptación seguros.
- Interfaz de usuario intuitiva y fácil de usar.

## Requisitos

- Navegador web moderno que admita HTML5 y JavaScript.

## Uso

1. Clona o descarga este repositorio en tu máquina local.
2. Abre el archivo `index.html` en tu navegador web.
3. Ingresa el texto que deseas encriptar en el campo de entrada.
4. Selecciona el algoritmo de encriptación deseado.
5. Haz clic en el botón "Encriptar".
6. El texto encriptado se mostrará en la pantalla.

## Contribución

Si deseas contribuir a este proyecto, puedes seguir estos pasos:

1. Haz un fork de este repositorio.
2. Crea una rama con tu nueva funcionalidad: `git checkout -b nueva-funcionalidad`.
3. Realiza los cambios necesarios y realiza los commits: `git commit -m "Agrega nueva funcionalidad"`.
4. Envía tus cambios al repositorio remoto: `git push origin nueva-funcionalidad`.
5. Abre una solicitud de extracción en GitHub.

## Licencia

Este proyecto está bajo la Licencia MIT. 

## Contacto

Si tienes alguna pregunta o sugerencia, no dudes en contactarme a través de mi dirección de correo electrónico: [adrianaparedessalinas@gamil.com](mailto:adrianaparedessalinas@gmail.com).
